# Otázky na prezentaci

| otázka | vyjádření |
| :--- | :--- |
| jak dlouho mi tvorba zabrala - **čistý čas** | 15 hodin|
| jak se mi to podařilo rozplánovat | šlo by to i lepe|
| design zapojení | https://gitlab.spseplzen.cz/hohausl/vlastni_shield_hohaus/-/raw/main/dokumentace/design/Design.png|
| proč jsem zvolil tento design | jednoduchý|
| zapojení | https://gitlab.spseplzen.cz/hohausl/vlastni_shield_hohaus/-/raw/main/dokumentace/Schema/schema_2.PNG  |
| z jakých součástí se zapojení skládá | 3x LED, ARGB pásek WS2812, LCD 1602 I2C display, DS18B20 čidlo kabel, fotorezistor, předpřipravená deska shield, rezistory|
| realizace | https://gitlab.spseplzen.cz/hohausl/vlastni_shield_hohaus/-/raw/main/dokumentace/fotky/IMG_6515.jpg |
| nápad, v jakém produktu vše propojit dohromady| - |
| co se mi povedlo | dát to dohromady, krabička |
| co se mi nepovedlo/příště bych udělal/a jinak | použít kratší drátky |
| zhodnocení celé tvorby | jsem docela spokojený |
